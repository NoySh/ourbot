def do_turn(pw):
	planets = pw.planets()
	biggest_growth = 0
	dest = planets
	for planet in planets:
		growth = planet.growth_rate()
		if (growth >biggest_growth):
			biggest_growth = growth
			dest = planet
	

	if len(pw.my_fleets()) >= 1:
		return

	if len(pw.my_planets()) == 0:
		return

#if len(pw.neutral_planets()) >= 1:
#		dest = pw.neutral_planets()[0]
	else:
		if len(pw.enemy_planets()) >= 1:
			dest = pw.enemy_planets()[0]

	source = pw.my_planets()[0]
	
	num_ships = source.num_ships() / 2
	pw.debug('Num Ships: ' + str(num_ships))

	pw.issue_order(source, dest, num_ships)
